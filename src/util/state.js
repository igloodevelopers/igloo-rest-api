export default class extends EventEmitter{
    constructor(conf){
        super();
        this.state = {
            buttonState:{

            },
            warperState:{
                banks:[

                ]

            }
        }
    }
    getState = () => {
        return this.state;
    }
    getBankClips = (bankNumber) =>{
        return this.state.warperState.banks[bankNumber];
    }
}