
import Express from "express";
import bodyParser from "body-parser";
import { EventEmitter } from "events";

export default class extends EventEmitter{

    constructor(conf,state){
        //setting up global variables
        super();
        //init express (the library for html routing)
        this.express = Express();
        //create global versions of conf and state
        this.conf = conf;
        this.stateServer = state;

        //allow put verbs
        this.express.use(bodyParser.urlencoded({extended:true}));
        this.express.use(bodyParser.json());

        // ----------- ROUTING ----------- //
        //setup routers
        //root router
        this.mainRouter = this.express.Router();
        //first level routers
        this.gamingRouter = this.express.Router({mergeParams:true});
        this.playbackRouter = this.express.Router({mergeParams:true});
        this.clipsRouter = this.express.Router({mergeParams:true});
        this.effectsRouter = this.express.Router({mergeParams:true});
        this.utilitiesRouter = this.express.Router({mergeParams:true});
        this.imageRouter = this.express.Router({mergeParams:true});
        this.warpingRouter = this.express.Router({mergeParams:true});

        //gaming sub-routers
        this.gaming3D = this.express.Router();
        this.gameEngine = this.express.Router();

       
        //First Level Routers   
        this.mainRouter.use("/gaming",this.gamingRouter);
        this.mainRouter.use("/clips",this.clipsRouter);
        this.mainRouter.use("/image",this.imageRouter);
        this.mainRouter.use("/playback",this.playbackRouter);
        this.mainRouter.use("/utilities",this.utilitiesRouter);
        this.mainRouter.use("/warping",this.warpingRouter);
        
        //Sub-Routers -------
        //Gaming ------------
        this.gamingRouter.use("/3D",this.gaming3D);
        this.gamingRouter.use("/gameEngine",this.gameEngine);

        // -----------  ROUTES  ---------- //
        //Root Resources ----
        this.mainRouter.route("/")
            .get((req,res)=>{
                res.status(200)
                    .send("Connected Successfully. Docs are available at " + this.conf.express.docs);
            });
        this.mainRouter.route("/status")
            .get((req,res)=>{
                res.json(this.stateServer.getState());
            });
        
        //Clip Routes -------
        //getBank
        this.clipsRouter.route("/bank/:bankNumber")
            .get((req,res)=>{
                try{
                   var bankNumber = req.params.bankNumber; 
                   var bankData = stateMachine.getBankClips(bankNumber);
                   res.status(200)
                        .json(response("ok",bankData));
                }
                catch(e){
                    this.emit('error',e);
                    res.status(500)
                        .send(this.response("error",500));
                }
            });
        
        //triggerClip
        this.clipsRouter.route("/trigger")
            .put((req,res)=>{
                this.emit('logging',req.body);
                try{
                    if(this.body.clip == null || this.body.bank == null){
                        this.emit(error,"Error 001");
                        res.status(500)
                            .send(this.response("error",1));
                    } else {
                        this.emit(sendOSC,{"command":"triggerClip","args":this.body});
                        res.status(200)
                            .send(this.response("ok",null));
                    }
                } catch (e){
                    this.emit('error',e);
                    res.status(500)
                        .send(this.response("error",500));
                }
            });
        
        
        this.effectsRouter.route("/mask/:maskNumber/display")
            .get((req,res)=>{

            })
            .put((req,res)=>{

            });
        


        //set the default route
        this.express.use('/api', router.mainRouter);
        
        
    }

    open = () => {};
    response = (state,body) => {

    }

    //error codes
    // 001: empty/missing parameters

    // 500: unknown server error

}