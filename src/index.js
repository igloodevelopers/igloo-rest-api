import StateMachine from './util/state';
import conf from './res/conf';
import Rest from './util/rest';
import OSCHandler from './util/oscHandler';

const stateTracker = new StateMachine(conf);
const restServer = new Rest(conf,stateTracker);
const osc = new OSCHandler(conf,stateTracker);

//rest server events
restServer.on('error', console.log);
restServer.on('sendOsc',(params) => osc.sendOSC(params));
restServer.on('status',console.log);
restServer.on('logging',console.log);

restServer.open();
